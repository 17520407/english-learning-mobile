import * as React from 'react';
import {LoadAssets, theme} from './src/components';
import {ThemeProvider} from '@shopify/restyle';
import {Provider, connect, useSelector} from 'react-redux';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import store from './src/config/store';
import IStore, {StoreProps} from './src/redux/model/store/IStore';
import {RootRoutes} from './src/navigation';
import {AppNavigator, AuthenticationNavigator} from './src/screens';

const fonts = {
  'DinRoundPro-Black': require('./assets/fonts/DINRoundPro-Black.ttf'),
  'DinRoundPro-Bold': require('./assets/fonts/DINRoundPro-Bold.ttf'),
  'DinRoundPro-Medi': require('./assets/fonts/DINRoundPro-Medi.ttf'),
  'DinRoundPro-Light': require('./assets/fonts/DINRoundPro-Light.ttf'),
};

const RootStack = createStackNavigator<RootRoutes>();

const RootNavigator = () => {
  const AuthState = useSelector((state: IStore) => state.Auth);
  React.useEffect(() => {}, [AuthState.accessToken]);

  return (
    <RootStack.Navigator headerMode="none">
      {AuthState.accessToken ? (
        <RootStack.Screen name="App" component={AppNavigator} />
      ) : (
        <RootStack.Screen
          name="Authentication"
          component={AuthenticationNavigator}
        />
      )}
    </RootStack.Navigator>
  );
};

export default function App() {
  return (
    <ThemeProvider {...{theme}}>
      <Provider store={store}>
        <LoadAssets fonts={fonts}>
          <SafeAreaProvider>
            <RootNavigator />
          </SafeAreaProvider>
        </LoadAssets>
      </Provider>
    </ThemeProvider>
  );
}
