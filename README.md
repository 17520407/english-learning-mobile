# Link source code back-end server :
https://gitlab.com/17520897/mobile-english-server
# Link source code front-end :
https://gitlab.com/17520407/english-learning-mobile
# Link của ứng dụng trên CH play :
https://play.google.com/store/apps/details?id=com.phiduyvo.CatLearningApp

# Cat English 
Ứng dụng học và thi tiếng anh

![alt text](https://gitlab.com/17520407/english-learning-mobile/-/raw/master/assets/cat_icon.png)

# Chức năng chính
## Phân quyền người dùng (đăng nhập ,đăng ký )
![alt text](https://gitlab.com/17520407/english-learning-mobile/-/raw/master/picture/%C4%91%C4%83ng%20nh%E1%BA%ADp%202.png)
![alt text](https://gitlab.com/17520407/english-learning-mobile/-/raw/master/picture/%C4%91%C4%83ng%20nh%E1%BA%ADp.png)
## Quản lý thông tin người dùng ,có thể thay đổi ảnh đại diện
![alt text](https://gitlab.com/17520407/english-learning-mobile/-/raw/master/picture/qu%E1%BA%A3n%20l%C3%BD%20th%C3%B4ng%20tin%20user.png)
## làm bài kiểm tra tiếng Anh
![alt text](https://gitlab.com/17520407/english-learning-mobile/-/raw/master/picture/m%C3%A0n%20h%C3%ACnh%20b%C3%A0i%20ki%E1%BB%83m%20tra.png)
## Học phát âm và từ vựng tiếng Anh
![alt text](https://gitlab.com/17520407/english-learning-mobile/-/raw/master/picture/h%E1%BB%8Dc%20ph%C3%A1t%20%C3%A2m.png)

# Thư viện và công nghệ
 - [**Expo**](https://expo.io/)
 - [**Axios**](https://github.com/axios/axios)
 - [**React Native**](https://reactnative.dev/)
 - [**Redux**](https://redux.js.org/)
 - [**Redux- Saga**](https://redux-saga.js.org/)
 - [**Babel**](https://babeljs.io/)

# Backend 
 - [**NodeJS**](https://nodejs.org/en/)

# Database 
 - [**MongoDB**](https://www.mongodb.com/)

# Diagram
## Sơ đồ phân tích của toàn bộ hệ thống
![alt text](https://gitlab.com/17520407/english-learning-mobile/-/raw/master/picture/diagram.png)

# Yêu cầu thiết bị
-Android:

   +Android studio : 3.0.1

   +Gradle : 4.1

   +Min sdk : 19

   +Target sdk : 29

   +Android : 5.0

-iOS:

   +iOS: 8.0

# Tác giả
Võ Phi Nhật Duy - 17520407@gm.uit.edu.vn

Trương Viết Huy Phong - 17520897@gm.uit.edu.vn

Nguyễn Quốc An - 16520012@gm.uit.edu.vn

# Giấy phép

    Copyright 2014

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
