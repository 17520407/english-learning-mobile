import React from 'react';
import {Formik, FormikHelpers} from 'formik';
import {Alert} from 'react-native';
import * as yup from 'yup';
import {
  Box,
  Text,
  TextInput,
  Button,
  theme,
} from '../../../../../../components';
import {ProfileProps} from '../../Profile';
import {useDispatch, useSelector} from 'react-redux';
import {updateUserInfo, userLogOut} from '../../../../../../redux/actions/app';
import {handleClear} from '../../../../../../redux/actions/authentication';
import IStore from '../../../../../../redux/model/store/IStore';

const validationProfileSchema = yup.object().shape({
  name: yup.string().required('Tên là trường bắt buộc'),
  email: yup
    .string()
    .email('Email không hợp lệ')
    .required('Email là trường bắt buộc'),
  password: yup.string().required('Mật khẩu là trường bắt buộc'),
});

interface FormUpdateInfoValues {
  name: string;
  email: string;
  password: string;
}

const UpdateInfoForm: React.FC<ProfileProps> = ({navigation}) => {
  const AppState = useSelector((state: IStore) => state.App);
  const dispatch = useDispatch();

  const {userInfo} = AppState;

  const initialValues = {
    name: userInfo ? userInfo.name : '',
    email: userInfo ? userInfo.email : '',
    password: '',
  };

  const onSubmit = (
    values: FormUpdateInfoValues,
    {setSubmitting}: FormikHelpers<FormUpdateInfoValues>,
  ) => {
    dispatch(updateUserInfo(values));
  };

  const showError = (errors: string, setErrors: () => void) => {
    Alert.alert('Thông báo', errors, [
      {
        text: 'Ok',
        onPress: setErrors,
      },
    ]);
  };

  return (
    <React.Fragment>
      <Text variant="body" marginBottom="m">
        Thông tin cá nhân
      </Text>
      <Formik
        validateOnBlur={false}
        validateOnChange={true}
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationProfileSchema}>
        {({handleChange, handleSubmit, errors, values, isValid}) => (
          <>
            <Box marginBottom="l">
              <TextInput
                label="Tên"
                value={values.name}
                placeholder="Tên"
                onChangeText={handleChange('name')}
              />
              <TextInput
                label="Email"
                value={values.email}
                placeholder="Email"
                onChangeText={handleChange('email')}
              />
              <Box position="relative">
                <Box
                  onTouchStart={() => navigation.navigate('ChangePassword')}
                  position="absolute"
                  top={0}
                  left={0}
                  right={0}
                  bottom={0}
                  style={{
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'transparent',
                    zIndex: 10,
                  }}></Box>
                <TextInput
                  label="Mật khẩu"
                  value="12345678"
                  placeholder="Password"
                  secureTextEntry={true}
                  editable={false}
                />
              </Box>
              {!isValid && (
                <Box marginBottom="m">
                  <Text
                    variant="body"
                    color="persianRed"
                    fontSize={15}
                    letterSpacing={2}>
                    {errors[Object.keys(errors)[0]]}
                  </Text>
                </Box>
              )}
              <Box marginBottom="m">
                <Button
                  variant="secondary"
                  label="Cập nhật"
                  onPress={handleSubmit}
                  disabled={!isValid}
                />
              </Box>
              <Box marginBottom="m">
                <Button
                  variant="default"
                  textColor="secondary"
                  label="Đăng xuất"
                  onPress={() => {
                    dispatch(userLogOut());
                    dispatch(handleClear({type: 'all'}));
                  }}
                />
              </Box>
            </Box>
          </>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default UpdateInfoForm;
