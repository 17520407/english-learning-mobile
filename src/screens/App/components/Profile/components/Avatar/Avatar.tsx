import React from 'react';
import {
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import {Box, theme} from '../../../../../../components';
import {Feather} from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import {useDispatch} from 'react-redux';
import {uploadFiles} from '../../../../../../redux/actions/app';
import {API_URL} from '../../../../../../common/constants';
interface AvatarProps {
  avatar: string;
}

const IMAGE_WIDTH = 80;
const WRAPPER_ICON_WIDTH = 24;

const Avatar: React.FC<AvatarProps> = ({avatar}) => {
  const [image, setImage] = React.useState(undefined);
  const dispatch = useDispatch();

  // React.useEffect(() => {
  //   if (image) {
  //     dispatch(uploadFiles({file: image}));
  //     setImage(undefined);
  //   }
  // }, [image]);

  React.useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const {status} = await ImagePicker.requestCameraPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });
    await setImage(result);
  };

  return (
    <Box style={styles.avatar}>
      <TouchableWithoutFeedback onPress={pickImage}>
        <Image
          resizeMode="contain"
          style={{height: '100%', width: '100%', borderRadius: IMAGE_WIDTH / 2}}
          source={{
            uri: avatar
              ? `http://staging.pd-mobile-web.tk${avatar}`
              : 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200',
          }}
        />
      </TouchableWithoutFeedback>
      <Box style={styles.iconEdit} right={0} top={0}>
        <Feather name="edit-2" size={12} color="white" />
      </Box>
    </Box>
  );
};

export default Avatar;

const styles = StyleSheet.create({
  avatar: {
    position: 'relative',
    width: IMAGE_WIDTH,
    height: IMAGE_WIDTH,
    borderRadius: IMAGE_WIDTH / 2,
  },
  iconEdit: {
    position: 'absolute',
    width: WRAPPER_ICON_WIDTH,
    height: WRAPPER_ICON_WIDTH,
    borderRadius: WRAPPER_ICON_WIDTH / 2,
    backgroundColor: theme.colors.secondary,
    borderWidth: 2,
    borderColor: theme.colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
