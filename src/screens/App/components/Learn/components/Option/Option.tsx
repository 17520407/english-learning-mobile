import React from 'react';
import {StyleSheet, Image, Dimensions} from 'react-native';
import {Box, Text, theme} from '../../../../../../components';
import {IAnswer} from '../../../../../../redux/model/IApp';
import {BASE_URL} from '../../../../../../common/constants';
import {Audio} from 'expo-av';

export interface AnswerOptionProps {
  item: IAnswer;
  onSelectAnswer: () => void;
  selected: boolean;
  onlyLabel?: boolean;
  inline?: boolean;
}
const {width, height} = Dimensions.get('window');

const AnswerOption = ({
  item,
  selected,
  onlyLabel,
  inline,
  onSelectAnswer,
}: AnswerOptionProps) => {
  const sound = new Audio.Sound();

  React.useEffect(() => {
    return sound
      ? () => {
          console.log('Unloading Sound');
          sound.unloadAsync();
        }
      : undefined;
  }, [sound]);

  async function playSound(url: string) {
    await sound.loadAsync({
      uri: url,
    });

    sound.playAsync();
  }

  return (
    <Box
      style={[
        styles.answerOptionContainer,
        selected ? styles.selected : {},
        onlyLabel ? {height: 120} : {},
        inline
          ? {
              height: 60,
              width: width * 0.34 - theme.spacing.l,
            }
          : {},
      ]}
      onTouchStart={() => {
        playSound(`${BASE_URL}${item.audio}`);
        onSelectAnswer();
      }}>
      <Box style={styles.answerOption}>
        {!onlyLabel && (
          <Box flex={3 / 4} style={styles.icon}>
            <Image
              resizeMode="stretch"
              style={{
                width: 100,
                height: 100,
              }}
              source={{
                uri: item.image,
              }}
            />
          </Box>
        )}
        <Box style={onlyLabel ? styles.onlyLabel : styles.label}>
          <Text variant="title2">{item.answer}</Text>
        </Box>
      </Box>
    </Box>
  );
};

export default AnswerOption;

const styles = StyleSheet.create({
  answerOptionContainer: {
    width: width * 0.48 - theme.spacing.l,
    height: height * 0.36 - theme.spacing.l,
    paddingHorizontal: theme.spacing.s,
    paddingVertical: theme.spacing.l,
    marginBottom: theme.spacing.m,
    borderRadius: theme.borderRadius.m,
    borderWidth: 2,
    borderBottomWidth: 4,
    borderColor: theme.colors.borderColorDefault,
    justifyContent: 'center',
  },
  selected: {
    borderColor: theme.colors.borderColorSecondary,
    backgroundColor: theme.colors.iguana,
    borderBottomWidth: 2,
  },
  answerOption: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    flex: 1 / 4,
    justifyContent: 'flex-end',
  },
  onlyLabel: {
    flex: 1,
    justifyContent: 'center',
  },
  icon: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
