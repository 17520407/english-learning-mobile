import React from 'react';
import {Box, Text, Button} from '../../../../../../components';
import {IAnswer, IUserAnswer} from '../../../../../../redux/model/IApp';

interface IFooterProps {
  result: IUserAnswer;
  onPressContinue: () => void;
  onCheckAnswer: () => void;
  selectedAnswer: IAnswer;
}

const Footer: React.FC<IFooterProps> = ({
  result,
  selectedAnswer,
  onCheckAnswer,
  onPressContinue,
}) => {
  console.log('selectedAnswer', selectedAnswer);
  const answerStatus = () => {
    if (result !== undefined) {
      if (result.rightAnswer === result.userAnswer) {
        return 'right';
      }
      return 'wrong';
    }
    return 'notSubmit';
  };

  if (!result) {
    return (
      <Box flex={1} justifyContent="flex-end" padding="m" marginBottom="l">
        <Button
          disabled={
            !selectedAnswer || (selectedAnswer && !selectedAnswer.answer)
          }
          variant="primary"
          label="Kiểm tra"
          onPress={onCheckAnswer}
        />
      </Box>
    );
  }

  return (
    <Box
      flex={1}
      justifyContent="flex-end"
      padding="m"
      paddingTop={answerStatus() === 'right' ? 'm' : 'xl'}
      marginBottom="l"
      backgroundColor={
        answerStatus() === 'right' ? 'seaSponge' : 'walkingFish'
      }>
      {answerStatus() === 'right' ? (
        <Box marginBottom="l">
          <Text variant="title" marginBottom="s" color="turtle">
            Chính xác
          </Text>
        </Box>
      ) : (
        <Box marginBottom="l">
          <Text variant="title" marginBottom="s" color="persianRed">
            Câu trả lời đúng:
          </Text>
          <Text variant="body" color="persianRed">
            {result && result.rightAnswer}
          </Text>
        </Box>
      )}

      <Button
        variant={answerStatus() === 'right' ? 'primary' : 'danger'}
        label="Tiếp tục"
        onPress={onPressContinue}
      />
    </Box>
  );
};

export default Footer;
