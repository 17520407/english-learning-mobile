import IStore, {StoreProps} from '../../../../redux/model/store/IStore';
import * as appActions from '../../../../redux/actions/app/actions';
import {Dispatch, bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Vocabulary from './Vocabulary';

function mapStateToProps(store: IStore) {
  return {
    store: store,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    actions: bindActionCreators(appActions, dispatch),
  };
}

export interface AppProps extends StoreProps<typeof appActions> {}

export default connect(mapStateToProps, mapDispatchToProps)(Vocabulary);
