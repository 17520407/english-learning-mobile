import React from 'react';
import {
  StyleSheet,
  ScrollView,
  NativeScrollEvent,
  NativeSyntheticEvent,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Box, Text, HeaderBar, theme} from '../../../../components';
import {AppRoutes, StackNavigationProps} from '../../../../navigation';
import LessonItem from './components/LessonItem';
import {AppProps} from './LessonContainer';

type LessonNavigation = StackNavigationProps<AppRoutes, 'Home'>;
export interface LessonProps extends LessonNavigation, AppProps {}

const Lesson = ({navigation, actions, store}: LessonProps) => {
  const {lessonRecords, isGettingLesson, userInfo} = store.App;
  const [contentOffsetY, setContentOffsetY] = React.useState(0);

  React.useEffect(() => {
    actions.getLessons({page: 0, limit: 10, maxLevel: 3});
  }, []);

  const enableLevel = (level: number) => {
    return userInfo?.levelInfo.level >= level;
  };

  const onScroll = (e: NativeSyntheticEvent<NativeScrollEvent>) => {
    const contentOffset = e.nativeEvent.contentOffset.y;
    // contentOffsetY < contentOffset
    // ? console.log('Scroll Down')
    // : console.log('Scroll Up');
    setContentOffsetY(contentOffset);
  };

  const renderLesson = () => {
    return Object.keys(lessonRecords).map((level) => {
      return (
        <Box
          key={`level_${level}`}
          justifyContent="center"
          alignItems="center"
          marginBottom="m"
          paddingTop="m"
          paddingBottom="l"
          marginHorizontal="m"
          borderColor="alto"
          backgroundColor={
            enableLevel(parseInt(level, 10))
              ? 'transparent'
              : 'borderColorDefault'
          }
          borderWidth={1}
          style={{
            borderRadius: 16,
          }}>
          {Object.keys(lessonRecords[level]).map((order) => {
            return (
              <Box
                key={`order_${order}`}
                flexDirection="row"
                justifyContent="space-evenly"
                width="100%"
                marginBottom="l">
                {lessonRecords[level][order].map((lesson) => (
                  <LessonItem
                    key={`lesson_${lesson._id}`}
                    onPress={() => {
                      if (enableLevel(parseInt(level, 10))) {
                        navigation.navigate('Learn', {lessonId: lesson._id});
                        actions.handleCurrentLesson({
                          type: 'detail',
                          lesson,
                        });
                      }
                    }}
                    item={lesson}
                  />
                ))}
              </Box>
            );
          })}
        </Box>
      );
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <HeaderBar label="Bài học" showBackButton={false} />
      {isGettingLesson ? (
        <Box flex={1} justifyContent="center" alignContent="center">
          <Text variant="title" textAlign="center">
            Loading...
          </Text>
        </Box>
      ) : (
        <Box flex={2}>
          <ScrollView
            style={{
              flex: 1,
              paddingTop: theme.spacing.m,
              backgroundColor: 'white',
            }}
            onScroll={onScroll}>
            {lessonRecords !== undefined && renderLesson()}
          </ScrollView>
        </Box>
      )}
    </SafeAreaView>
  );
};

export default Lesson;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
});
