import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {Box, Text, theme} from '../../../../../../components';
import {ILesson} from '../../../../../../redux/model/IApp';
export interface LessonItemProps {
  item: ILesson;
  onPress: () => void;
}

export const LESSON_ITEM_WIDTH = 114;
export const LESSON_ICON_WIDTH = LESSON_ITEM_WIDTH - theme.spacing.xs;

const LessonItem: React.FC<LessonItemProps> = ({item, onPress}) => {
  const {icon, isLearn, name} = item;
  const lessonBackground = isLearn
    ? theme.colors.borderColorSecondary
    : theme.colors.borderColorDefault;

  return (
    <Box style={styles.container}>
      <Box style={styles.iconWrapper}>
        <Box
          style={[styles.iconInner, {backgroundColor: lessonBackground}]}
          onTouchStart={onPress}>
          <Image
            resizeMode="center"
            style={{
              width: 50,
              height: 50,
            }}
            source={{
              uri: icon,
            }}
          />
        </Box>
      </Box>
      <Box
        style={{
          display: 'flex',
          alignItems: 'center',
          position: 'relative',
        }}>
        <Box overflow="hidden" position="absolute" width={100}>
          <Text
            variant="title2"
            textAlign="center"
            textBreakStrategy={'simple'}>
            {name}
          </Text>
        </Box>
      </Box>
    </Box>
  );
};

export default LessonItem;

const styles = StyleSheet.create({
  container: {
    marginVertical: theme.spacing.m,
    // backgroundColor: 'black',
  },
  iconWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors.white,
    marginBottom: theme.spacing.xs,
    padding: theme.spacing.s,
    height: LESSON_ITEM_WIDTH,
    width: LESSON_ITEM_WIDTH,
    borderRadius: LESSON_ITEM_WIDTH * 0.5,
    borderWidth: 6,
    borderColor: theme.colors.borderColorDefault,
  },
  iconInner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: theme.colors.borderColorSecondary,
    borderRadius: LESSON_ITEM_WIDTH - theme.spacing.s * 2 * 0.5,
  },
});
