import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {AppRoutes} from '../../navigation';
import {ChangePassword, Learn} from './components';
import AppBottomNavigator from './AppBottomNavigator';
import {useDispatch} from 'react-redux';
import {getUserInfo} from '../../redux/actions/app/actions';

const AppStack = createStackNavigator<AppRoutes>();

const AppNavigator = () => {
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(getUserInfo());
  }, []);

  return (
    <React.Fragment>
      <AppStack.Navigator initialRouteName="Home" headerMode="none">
        <AppStack.Screen name="Home" component={AppBottomNavigator} />
        <AppStack.Screen name="Learn" component={Learn} />
        {/* <AppStack.Screen name="VocabularyLearn" component={VocabularyLearn} /> */}
        <AppStack.Screen name="ChangePassword" component={ChangePassword} />
      </AppStack.Navigator>
    </React.Fragment>
  );
};

export default AppNavigator;
