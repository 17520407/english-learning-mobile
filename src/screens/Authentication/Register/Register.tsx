import React from 'react';
import {StyleSheet} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Box, HeaderBar, Text, theme} from '../../../components';
import {Feather} from '@expo/vector-icons';
import {AuthenticationRoutes, StackNavigationProps} from '../../../navigation';
import {RegisterForm} from './components';

type RegisterNavigation = StackNavigationProps<
  AuthenticationRoutes,
  'Register'
>;
export interface RegisterProps extends RegisterNavigation {}
const Register: React.FC<RegisterProps> = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <Box flex={1} padding="m" position="relative">
        <HeaderBar
          border={false}
          label="Thông tin của bạn"
          onBackPress={() => navigation.navigate('GetStarted')}
        />
        <RegisterForm />
        <Box
          alignItems="center"
          flexDirection="row"
          justifyContent="center"
          alignContent="flex-end">
          <Text variant="title2" color="text" marginRight="s">
            Đã có tài khoản?
          </Text>
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate('Login')}>
            <Text variant="title2" textTransform="uppercase" color="secondary">
              đăng nhập
            </Text>
          </TouchableWithoutFeedback>
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
