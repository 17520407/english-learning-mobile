import React from 'react';
import {StyleSheet} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Box, Text, Button, theme, HeaderBar} from '../../../components';
import {Feather} from '@expo/vector-icons';
import {AuthenticationRoutes, StackNavigationProps} from '../../../navigation';
import {LoginForm} from './components';

type LoginNavigation = StackNavigationProps<AuthenticationRoutes, 'Login'>;
export interface LoginProps extends LoginNavigation {}
const Login: React.FC<LoginProps> = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <Box flex={1} padding="m" position="relative">
        <HeaderBar
          label="Đăng nhập"
          border={false}
          onBackPress={() => navigation.navigate('GetStarted')}
        />
        <LoginForm />
        <Box alignItems="center">
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate('ForgotPassword')}>
            <Text variant="title2" textTransform="uppercase" color="secondary">
              Quên mật khẩu?
            </Text>
          </TouchableWithoutFeedback>
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
