import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {AuthenticationRoutes} from '../../navigation';
import {OnBoarding} from './OnBoarding';
import {Login} from './Login';
import {GetStated} from './GetStarted';
import {Register} from './Register';
import {ForgotPassword} from './ForgotPassword';
import {bindActionCreators, Dispatch} from 'redux';
import IStore, {StoreProps} from '../../redux/model/store/IStore';
import * as authActions from '../../redux/actions/authentication/actions';
import {connect} from 'react-redux';
import {getItem} from '../../utils/AsyncStorage';

const AuthenticationStack = createStackNavigator<AuthenticationRoutes>();

const AuthenticationNavigator: React.FC<AuthProps> = (props) => {
  React.useEffect(() => {
    const handleLoadLocalToken = async () => {
      try {
        const result = await getItem('authentication');
        if (result) {
          const token = JSON.parse(result);
          props.actions.getStoredAuthentication({
            accessToken: token.accessToken,
          });
        }
      } catch (error) {
        alert('Failed to fetch the data from storage');
      }
    };

    handleLoadLocalToken();
  }, []);

  return (
    <AuthenticationStack.Navigator
      headerMode="none"
      initialRouteName="OnBoarding">
      <AuthenticationStack.Screen name="OnBoarding" component={OnBoarding} />
      <AuthenticationStack.Screen name="GetStarted" component={GetStated} />
      <AuthenticationStack.Screen name="Login" component={Login} />
      <AuthenticationStack.Screen name="Register" component={Register} />
      <AuthenticationStack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
      />
    </AuthenticationStack.Navigator>
  );
};

function mapStateToProps(store: IStore) {
  return {
    store: store,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    actions: bindActionCreators(authActions, dispatch),
  };
}
export interface AuthProps extends StoreProps<typeof authActions> {}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthenticationNavigator);
