import {request} from '../config/axios';

export const userLogin = (data: any) => {
  const endpoint = '/users/login';
  return request(endpoint, 'POST', data);
};

export const userRegister = (data: any) => {
  const endpoint = '/users/register';
  return request(endpoint, 'POST', data);
};
