export const name = 'App';

export interface IAnswer {
  _id: string;
  answer: string;
  image: string;
  audio: string;
}
export interface IQuestion {
  _id: string;
  type: 'pictures' | 'listening' | 'sentence';
  question: string;
  link?: string;
  rightAnswer: string;
  answers: IAnswer[];
}
export interface ILesson {
  level: number;
  icon: string;
  order: number;
  requireOrder: number;
  _id: string;
  name: string;
  questions: IQuestion[];
  isLearn: boolean;
}

export interface IUserAnswer {
  questionId: string;
  question: string;
  userAnswer: string;
  rightAnswer: string;
}

export interface IUserInfo {
  _id?: string;
  levelInfo?: {level: number; order: number};
  email?: string;
  gamesPass?: any[];
  dayOfBirth?: string;
  gender?: string;
  name?: string;
  avatar?: string;
  password?: string;
  oldPassword: string;
}

export interface ILessonRecord {
  [x: string]: {
    [x: string]: ILesson[];
  };
}

export interface IVocabulary {
  audio: string;
  english: string;
  example: string;
  exampleTranslate: string;
  explain: string;
  image: string;
  spell: string;
  vietnamese: string;
  _id: string;
}

export interface IVocabularyPackage {
  isLeaf: boolean;
  name: string;
  ancestors: string[];
  packageImage: string;
  _id: string;
  vocabularies: {_id: string; vocabulary: IVocabulary}[];
}

export interface IUserHistory {
  date: string;
  isLogin: true;
  totalGameLevel: number;
  loginDay?: number;
  totalVocabularyPackages: number;
  userId: string;
  _id: string;
}

export interface IPagination {
  totalPage?: number;
  totalRecord?: number;
}

export interface IAppState {
  isGettingInfo: boolean;
  isGettingLesson: boolean;
  isProcessing: boolean;
  isLoadingHistory: boolean;
  isUploadSuccessful: boolean;
  isHistoryCreatedSuccessful: boolean;
  isLoadingVocabularyPackage: boolean;
  userInfo?: IUserInfo;
  userAnswers?: IUserAnswer[];
  currentLesson?: ILesson;
  vocabularyPackages: {
    data: IVocabularyPackage[];
    pagination: IPagination;
  };
  lessonRecords?: ILessonRecord;

  userHistoryRecords: IUserHistory[];
  pagination: IPagination;
}

// InitialState
export const initialState: IAppState = {
  userHistoryRecords: [],
  isGettingInfo: false,
  isProcessing: false,
  isGettingLesson: false,
  isLoadingHistory: false,
  isUploadSuccessful: false,
  isLoadingVocabularyPackage: false,
  isHistoryCreatedSuccessful: false,
  vocabularyPackages: {
    data: [],
    pagination: {
      totalRecord: 0,
      totalPage: 0,
    },
  },
  pagination: {
    totalPage: 0,
    totalRecord: 0,
  },
};
