/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
  | IActions.IHandleCurrentLesson
  | IActions.IHandleClear
  | IActions.IUserLogOut
  | IActions.IUploadFiles
  | IActions.IUploadFilesSuccess
  | IActions.IUploadFilesFail
  | IActions.IGetLessons
  | IActions.IGetLessonsSuccess
  | IActions.IGetLessonsFail
  | IActions.IGetUserHistory
	| IActions.IGetUserHistorySuccess
	| IActions.IGetUserHistoryFail
  | IActions.IPostUserHistory
  | IActions.IPostUserHistorySuccess
  | IActions.IPostUserHistoryFail
  | IActions.IGetLessonById
  | IActions.IGetLessonByIdSuccess
  | IActions.IGetLessonByIdFail
  | IActions.IGetUserInfo
  | IActions.IGetUserInfoSuccess
  | IActions.IGetUserInfoFail
  | IActions.IUpdateUserInfo
  | IActions.IUpdateUserInfoSuccess
  | IActions.IUpdateUserInfoFail
  | IActions.IGetVocabularyPackages
  | IActions.IGetVocabularyPackagesSuccess
  | IActions.IGetVocabularyPackagesFail
  | IActions.IGetVocabulary
  | IActions.IGetVocabularySuccess
  | IActions.IGetVocabularyFail;

export default ActionTypes;
