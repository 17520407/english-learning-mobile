import {findIndex} from 'core-js/fn/array';
import {Alert} from 'react-native';
import {Reducer} from 'redux';
import {ErrorCodeMessages} from '../../common/constants';
import {Keys, ActionTypes} from '../actions/authentication';
import * as IActions from '../actions/authentication/IActions';
import {IAuthenticationState, initialState} from '../model/IAuthentication';

export const name = 'Auth';
const reducer: Reducer<IAuthenticationState, ActionTypes> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case Keys.HANDLE_CLEAR:
      return onHandleClear(state, action);
    case Keys.GET_STORED_AUTHENTICATION:
      return onGetStoredAuthentication(state, action);
    case Keys.USER_LOGIN:
      return onUserLogin(state, action);
    case Keys.USER_LOGIN_SUCCESS:
      return onUserLoginSuccess(state, action);
    case Keys.USER_LOGIN_FAIL:
      return onUserLoginFail(state, action);
    default:
      return state;
  }
};

// IActions: the interface of current action

const onHandleClear = (
  state: IAuthenticationState,
  action: IActions.IHandleClear,
) => {
  const {type} = action.payload;
  switch (type) {
    case 'all':
      return {
        ...state,
        accessToken: null,
      };
    default:
      return {
        ...state,
      };
  }
};

const onGetStoredAuthentication = (
  state: IAuthenticationState,
  action: IActions.IGetStoredAuthentication,
) => {
  const {accessToken} = action.payload;
  return {
    ...state,
    accessToken,
  };
};

const onUserLogin = (
  state: IAuthenticationState,
  action: IActions.IUserLogin,
) => {
  return {
    ...state,
    isProcessing: true,
  };
};
const onUserLoginSuccess = (
  state: IAuthenticationState,
  action: IActions.IUserLoginSuccess,
) => {
  const {accessToken, refreshToken} = action.payload;
  return {
    ...state,
    isProcessing: false,
    accessToken,
    refreshToken,
  };
};
const onUserLoginFail = (
  state: IAuthenticationState,
  action: IActions.IUserLoginFail,
) => {
  const index = ErrorCodeMessages.findIndex(
    (error) => error.code === action.payload[0].code,
  );
  if (index > -1) {
    Alert.alert('Lỗi', ErrorCodeMessages[index].error);
  }
  return {
    ...state,
    isProcessing: false,
  };
};
export default reducer;
