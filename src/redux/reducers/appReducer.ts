import {Reducer} from 'redux';
import {Keys, ActionTypes} from '../actions/app';
import * as IActions from '../actions/app/IActions';
import {IAppState, initialState} from '../model/IApp';

export const name = 'App';
const reducer: Reducer<IAppState, ActionTypes> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case Keys.HANDLE_CLEAR:
      return onHandleClear(state, action);
    case Keys.HANDLE_CURRENT_LESSON:
      return onHandleCurrentLesson(state, action);
    case Keys.USER_LOG_OUT:
      return onUserLogOut(state, action);

    case Keys.GET_LESSONS:
      return onGetLessons(state, action);
    case Keys.GET_LESSONS_SUCCESS:
      return onGetLessonsSuccess(state, action);
    case Keys.GET_LESSONS_FAIL:
      return onGetLessonsFail(state, action);

    case Keys.GET_USER_HISTORY:
      return onGetUserHistory(state, action);
    case Keys.GET_USER_HISTORY_SUCCESS:
      return onGetUserHistorySuccess(state, action);
    case Keys.GET_USER_HISTORY_FAIL:
      return onGetUserHistoryFail(state, action);

    case Keys.GET_VOCABULARY_PACKAGES:
      return onGetVocabularyPackages(state, action);
    case Keys.GET_VOCABULARY_PACKAGES_SUCCESS:
      return onGetVocabularyPackagesSuccess(state, action);
    case Keys.GET_VOCABULARY_PACKAGES_FAIL:
      return onGetVocabularyPackagesFail(state, action);

    case Keys.POST_USER_HISTORY:
      return onPostUserHistory(state, action);
    case Keys.POST_USER_HISTORY_SUCCESS:
      return onPostUserHistorySuccess(state, action);
    case Keys.POST_USER_HISTORY_FAIL:
      return onPostUserHistoryFail(state, action);

    case Keys.GET_LESSON_BY_ID:
      return onGetLessonById(state, action);
    case Keys.GET_LESSON_BY_ID_SUCCESS:
      return onGetLessonByIdSuccess(state, action);
    case Keys.GET_LESSON_BY_ID_FAIL:
      return onGetLessonByIdFail(state, action);

    case Keys.UPDATE_FILES:
      return onUploadFiles(state, action);
    case Keys.UPDATE_FILES_SUCCESS:
      return onUploadFilesSuccess(state, action);
    case Keys.UPDATE_FILES_FAIL:
      return onUploadFilesFail(state, action);

    case Keys.UPDATE_USER_INFO:
      return onUpdateUserInfo(state, action);
    case Keys.UPDATE_USER_INFO_SUCCESS:
      return onUpdateUserInfoSuccess(state, action);
    case Keys.UPDATE_USER_INFO_FAIL:
      return onUpdateUserInfoFail(state, action);

    case Keys.GET_USER_INFO:
      return onGetUserInfo(state, action);
    case Keys.GET_USER_INFO_SUCCESS:
      return onGetUserInfoSuccess(state, action);
    case Keys.GET_USER_INFO_FAIL:
      return onGetUserInfoFail(state, action);
    default:
      return state;
  }
};

// IActions: the interface of current action
const onHandleClear = (state: IAppState, action: IActions.IHandleClear) => {
  const {type} = action.payload;

  switch (type) {
    case 'update':
      return {
        ...state,
        updateInfoSuccess: false,
      };
    case 'postUserHistory':
      return {
        ...state,
        isHistoryCreatedSuccessful: false,
      };

    default:
      return {
        ...state,
      };
  }
};
const onHandleCurrentLesson = (
  state: IAppState,
  action: IActions.IHandleCurrentLesson,
) => {
  const {type, lesson} = action.payload;

  switch (type) {
    case 'detail':
      return {
        ...state,
        currentLesson: lesson,
      };

    default:
      return {
        ...state,
      };
  }
};

const onUserLogOut = (state: IAppState, action: IActions.IUserLogOut) => {
  return {
    ...state,
    userInfo: null,
    isGettingInfo: false,
  };
};

const onUpdateUserInfo = (
  state: IAppState,
  action: IActions.IUpdateUserInfo,
) => {
  return {
    ...state,
    updateInfoSuccess: false,
    isProcessing: true,
  };
};
const onUpdateUserInfoSuccess = (
  state: IAppState,
  action: IActions.IUpdateUserInfoSuccess,
) => {
  return {
    ...state,
    updateInfoSuccess: true,
    isProcessing: false,
  };
};
const onUpdateUserInfoFail = (
  state: IAppState,
  action: IActions.IUpdateUserInfoFail,
) => {
  return {
    ...state,
    updateInfoSuccess: false,
    isProcessing: false,
  };
};

const onPostUserHistory = (
  state: IAppState,
  action: IActions.IPostUserHistory,
) => {
  return {
    ...state,
    isProcessing: true,
  };
};
const onPostUserHistorySuccess = (
  state: IAppState,
  action: IActions.IPostUserHistorySuccess,
) => {
  return {
    ...state,
    isProcessing: false,
    isHistoryCreatedSuccessful: true,
  };
};
const onPostUserHistoryFail = (
  state: IAppState,
  action: IActions.IPostUserHistoryFail,
) => {
  return {
    ...state,
    isProcessing: false,
  };
};

const onUploadFiles = (state: IAppState, action: IActions.IUploadFiles) => {
  return {
    ...state,
  };
};
const onUploadFilesSuccess = (
  state: IAppState,
  action: IActions.IUploadFilesSuccess,
) => {
  return {
    ...state,
    isUploadSuccessful: true,
    userInfo: {
      ...state.userInfo,
      avatar: action.payload.url,
    },
  };
};
const onUploadFilesFail = (
  state: IAppState,
  action: IActions.IUploadFilesFail,
) => {
  return {
    ...state,
  };
};

const onGetLessons = (state: IAppState, action: IActions.IGetLessons) => {
  return {
    ...state,
    isGettingLesson: true,
  };
};
const onGetLessonsSuccess = (
  state: IAppState,
  action: IActions.IGetLessonsSuccess,
) => {
  return {
    ...state,
    isGettingLesson: false,
    lessonRecords: action.payload,
  };
};
const onGetLessonsFail = (
  state: IAppState,
  action: IActions.IGetLessonsFail,
) => {
  return {
    ...state,
    isGettingLesson: false,
  };
};

const onGetLessonById = (state: IAppState, action: IActions.IGetLessonById) => {
  return {
    ...state,
  };
};
const onGetLessonByIdSuccess = (
  state: IAppState,
  action: IActions.IGetLessonByIdSuccess,
) => {
  // const pagination = action.payload.pop();

  return {
    ...state,
  };
};
const onGetLessonByIdFail = (
  state: IAppState,
  action: IActions.IGetLessonByIdFail,
) => {
  return {
    ...state,
  };
};

const onGetUserInfo = (state: IAppState, action: IActions.IGetUserInfo) => {
  return {
    ...state,
    isGettingInfo: true,
  };
};
const onGetUserInfoSuccess = (
  state: IAppState,
  action: IActions.IGetUserInfoSuccess,
) => {
  return {
    ...state,
    isGettingInfo: false,
    userInfo: action.payload,
  };
};
const onGetUserInfoFail = (
  state: IAppState,
  action: IActions.IGetUserInfoFail,
) => {
  return {
    ...state,
    isGettingInfo: false,
  };
};

const onGetUserHistory = (
  state: IAppState,
  action: IActions.IGetUserHistory,
) => {
  return {
    ...state,
    isLoadingHistory: true,
  };
};
const onGetUserHistorySuccess = (
  state: IAppState,
  action: IActions.IGetUserHistorySuccess,
) => {
  return {
    ...state,
    isLoadingHistory: false,
    userHistoryRecords: action.payload,
    chartInfo: action.payload.pop(),
  };
};
const onGetUserHistoryFail = (
  state: IAppState,
  action: IActions.IGetUserHistoryFail,
) => {
  return {
    ...state,
    isLoadingHistory: false,
  };
};

const onGetVocabularyPackages = (
  state: IAppState,
  action: IActions.IGetVocabularyPackages,
) => {
  return {
    ...state,
    isLoadingVocabularyPackage: true,
  };
};
const onGetVocabularyPackagesSuccess = (
  state: IAppState,
  action: IActions.IGetVocabularyPackagesSuccess,
) => {
  return {
    ...state,
    isLoadingVocabularyPackage: false,
    vocabularyPackages: {
      data: action.payload,
      pagination: action.payload.pop(),
    },
  };
};
const onGetVocabularyPackagesFail = (
  state: IAppState,
  action: IActions.IGetVocabularyPackagesFail,
) => {
  return {
    ...state,
    isLoadingVocabularyPackage: false,
  };
};

export default reducer;
