import axios, {AxiosRequestConfig} from 'axios';
import {getItem} from '../utils/AsyncStorage';

export const contentType = {
  json: 'application/json',
  formData: 'multipart/form-data',
};

const callAPI = async (
  endpoint: string,
  method: AxiosRequestConfig['method'],
  data: any,
  formData = false,
  noneHeader = false,
  loadJson = false,
) => {
  // Check JSON Parse
  const authentication = await getItem('authentication');
  const baseUrl = loadJson
    ? endpoint
    : `https://staging.pd-mobile-web.tk/api${endpoint}`;
  const headers = {
    Authorization: noneHeader
      ? ''
      : `Bearer ${
          authentication ? JSON.parse(authentication).accessToken : ''
        }`,
    'Content-Type': formData ? contentType.formData : contentType.json,
    'Access-Control-Allow-Origin': '*',
  };

  const options: AxiosRequestConfig = {
    method,
    url: baseUrl,
    headers,
    data: method !== 'GET' ? data : null,
  };

  return axios(options).catch((error) => {
    return new Promise((resolve) => {
      return resolve(error.response);
    });
  });
};

export const request = async (
  endpoint: string,
  method: AxiosRequestConfig['method'],
  data: any,
  formData = false,
  noneHeader = false,
  loadJson = false,
) => {
  try {
    const res = (await callAPI(
      endpoint,
      method,
      data,
      formData,
      noneHeader,
      loadJson,
    )) as any;
    const {status} = res;
    if (status === 401) {
      return;
      // history.push("/login");
      // localStorage.clear();
      // window.location.reload();
    } else {
      return {
        data: res.data,
        status,
      };
    }
  } catch (error) {
    // tslint:disable-next-line: no-shadowed-variable
    const {status, data} = error.response;
    return {
      status,
      data,
    };
  }
};
