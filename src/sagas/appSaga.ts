import {call, put, fork, all, takeEvery} from 'redux-saga/effects';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Keys} from '../redux/actions/app';
import * as actions from '../redux/actions/app/actions';
import {handleClear} from '../redux/actions/authentication/actions';
import * as lessonApi from '../api/lesson';
import * as userApi from '../api/user';
import * as vocabularyApi from '../api/vocabulary';
import {Alert} from 'react-native';

function* handleUserLogOut(action: any) {
  try {
    yield AsyncStorage.removeItem('authentication');
    yield put(handleClear({type: 'all'}));
  } catch (err) {}
}

function* handleGetLessons(action: any) {
  try {
    const res = yield call(lessonApi.getLessons, action.payload);
    const {data} = res.data;
    if (res.status === 200) {
      yield put(actions.getLessonsSuccess(data));
    } else if (res.status === 401) {
      yield put(actions.userLogOut());
    } else {
      const {errors} = res.data;
      Alert.alert('Lỗi', errors[0].error);
      yield put(actions.getLessonsFail(data));
    }
  } catch (err) {
    yield put(actions.getLessonsFail(err));
  }
}

function* handleGetLessonById(action: any) {
  try {
    const res = yield call(lessonApi.getLessonById, action.payload);
    const {data} = res.data;
    if (res.status === 200) {
      yield put(actions.getLessonByIdSuccess(data));
    } else if (res.status === 401) {
      yield put(actions.userLogOut());
    } else {
      yield put(actions.getLessonByIdFail(data));
    }
  } catch (err) {
    yield put(actions.getLessonByIdFail(err));
  }
}

function* handleGetVocabularyPackages(action: any) {
  try {
    const res = yield call(vocabularyApi.getVocabularyPackages, action.payload);
    const {data} = res.data;
    if (res.status === 200) {
      yield put(actions.getVocabularyPackagesSuccess(data));
    } else if (res.status === 401) {
      yield put(actions.userLogOut());
    } else {
      yield put(actions.getVocabularyPackagesFail(data));
    }
  } catch (err) {
    yield put(actions.getVocabularyPackagesFail(err));
  }
}

function* handleUploadFiles(action: any) {
  try {
    const res = yield call(userApi.uploadImage, action.payload);
    const {data} = res.data;
    if (res.status === 200) {
      Alert.alert('Thông báo', 'Upload hình thành công');
      yield put(actions.uploadFilesSuccess(data));
    } else {
      yield put(actions.uploadFilesFail(data));
    }
  } catch (err) {
    yield put(actions.uploadFilesFail(err));
  }
}

function* handleGetVocabulary(action: any) {
  try {
    const res = yield call(vocabularyApi.getVocabularyPackages, action.payload);
    const {data} = res.data;
    if (res.status === 200) {
      yield put(actions.getVocabularySuccess(data));
    } else if (res.status === 401) {
      yield put(actions.userLogOut());
    } else {
      yield put(actions.getVocabularyFail(data));
    }
  } catch (err) {
    yield put(actions.getVocabularyFail(err));
  }
}

function* handleGetUserInfo(action: any) {
  try {
    const res = yield call(userApi.getUserInfo);
    const {data} = res.data;
    if (res.status === 200) {
      yield put(actions.getUserInfoSuccess(data));
    } else {
      yield put(actions.getUserInfoFail(data));
    }
  } catch (err) {
    yield put(actions.getUserInfoFail(err));
  }
}

function* handleUpdateUserInfo(action: any) {
  try {
    const res = yield call(userApi.updateUserInfo, action.payload);
    if (res.status === 200) {
      yield put(actions.updateUserInfoSuccess(res.data.data));
    } else {
      yield put(actions.updateUserInfoFail(res.data));
    }
  } catch (err) {
    yield put(actions.updateUserInfoFail(err));
  }
}

function* handleGetUserHistory(action: any) {
  try {
    const res = yield call(userApi.getUserHistory, action.payload);
    if (res.status === 200) {
      yield put(actions.getUserHistorySuccess(res.data.data));
    } else {
      yield put(actions.getUserHistoryFail(res.data));
    }
  } catch (error) {
    yield put(actions.getUserHistoryFail(error));
  }
}

function* handlePostUserHistory(action: any) {
  try {
    const res = yield call(userApi.postUserHistory, action.payload);
    if (res.status === 200) {
      yield put(actions.postUserHistorySuccess(res.data.data));
    } else {
      yield put(actions.postUserHistoryFail(res.data));
    }
  } catch (error) {
    yield put(actions.postUserHistoryFail(error));
  }
}
/*-----------------------------------------------------------------*/
export function* watchGetUserInfo() {
  yield takeEvery(Keys.GET_USER_INFO, handleGetUserInfo);
}
export function* watchUpdateUserInfo() {
  yield takeEvery(Keys.UPDATE_USER_INFO, handleUpdateUserInfo);
}
export function* watchPostUserHistory() {
  yield takeEvery(Keys.POST_USER_HISTORY, handlePostUserHistory);
}
export function* watchUserLogOut() {
  yield takeEvery(Keys.USER_LOG_OUT, handleUserLogOut);
}
export function* watchGetLessons() {
  yield takeEvery(Keys.GET_LESSONS, handleGetLessons);
}
function* watchGetUserHistory() {
  yield takeEvery(Keys.GET_USER_HISTORY, handleGetUserHistory);
}
function* watchUploadFiles() {
  yield takeEvery(Keys.UPDATE_FILES, handleUploadFiles);
}
// export function* watchGetLessonById() {
//   yield takeEvery(Keys.GET_LESSON_BY_ID, handleGetLessonById);
// }
export function* watchGetVocabularyPackages() {
  yield takeEvery(Keys.GET_VOCABULARY_PACKAGES, handleGetVocabularyPackages);
}
export function* watchGetVocabulary() {
  yield takeEvery(Keys.GET_VOCABULARY, handleGetVocabulary);
}

/*-----------------------------------------------------------------*/
const main = [
  watchGetUserInfo,
  watchUpdateUserInfo,
  watchGetUserHistory,
  watchPostUserHistory,
  watchUserLogOut,
  watchUploadFiles,
  watchGetLessons,
  watchGetVocabularyPackages,
  watchGetVocabulary,
];

export default function* appSaga() {
  yield all([...main.map((saga) => fork(saga))]);
}
