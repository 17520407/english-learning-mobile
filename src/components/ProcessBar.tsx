import React from 'react';
import {StyleSheet} from 'react-native';
import theme, {Box} from './Theme';

export interface ProcessBarProps {
  width: string;
}

const ProcessBar = ({width}: ProcessBarProps) => {
  return (
    <Box flex={1} style={styles.processBarContainer}>
      <Box style={[styles.processBar, {width}]}>
        <Box style={styles.processShadow} />
      </Box>
    </Box>
  );
};

export default ProcessBar;

const styles = StyleSheet.create({
  processBarContainer: {
    width: '100%',
    height: 20,
    backgroundColor: theme.colors.borderColorDefault,
    borderRadius: theme.borderRadius.s,
    position: 'relative',
    marginHorizontal: theme.spacing.xs,
  },
  processBar: {
    position: 'absolute',
    width: '0%',
    height: '100%',
    backgroundColor: theme.colors.primary,
    borderRadius: theme.borderRadius.s,
    top: 0,
    left: 0,
    zIndex: 2,
  },
  processShadow: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '80%',
    height: '30%',
    backgroundColor: theme.colors.white,
    borderRadius: theme.borderRadius.s,
    marginHorizontal: theme.spacing.m,
    marginTop: 5,
    zIndex: 4,
    opacity: 0.2,
  },
});
