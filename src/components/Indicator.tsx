import React from 'react';
import {StyleSheet, ActivityIndicator} from 'react-native';
import theme, {Box} from './Theme';

const Indicator = () => {
	return (
		<Box
			style={{
				...StyleSheet.absoluteFillObject,
				top: 0,
				left: 0,
				zIndex: 2,
			}}
			flex={1}
			justifyContent="center"
			alignItems="center"
			backgroundColor="doveGray">
			<Box
				justifyContent="center"
				alignItems="center"
				width={60}
				height={60}
				style={{borderRadius: 30}}
				backgroundColor="white"
				padding="l">
				<ActivityIndicator
					size="large"
					animating={true}
					color={theme.colors.secondary}
				/>
			</Box>
		</Box>
	);
};

export default Indicator;
