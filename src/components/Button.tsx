import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';
import theme from './Theme';

interface IButtonProps {
  variant: 'secondary' | 'primary' | 'danger' | 'default';
  textColor?: 'secondary' | 'primary';
  label: string;
  onPress: () => void;
  disabled?: boolean;
  loading?: boolean;
}

const Button = ({
  variant,
  textColor,
  label,
  disabled,
  loading,
  onPress,
}: IButtonProps) => {
  const color = variant !== 'default' ? 'white' : theme.colors.primary;

  const disableButton = disabled || loading;

  return (
    <RectButton
      onPress={disableButton ? () => null : onPress}
      style={[styles.wrapper]}>
      <View
        style={[
          styles.button,
          disableButton ? styles.disabled : styles[variant],
        ]}>
        <Text
          style={[
            styles.label,
            disableButton
              ? styles.labelDisabled
              : {color: textColor ? theme.colors[textColor] : color},
          ]}>
          {loading ? 'Đang xử lý' : label}
        </Text>
      </View>
    </RectButton>
  );
};

export default Button;

const styles = StyleSheet.create({
  wrapper: {
    minWidth: 150,
    width: '100%',
  },
  button: {
    borderRadius: 16,
    paddingHorizontal: theme.spacing.m,
    paddingVertical: 12,
    textAlign: 'center',
    textAlignVertical: 'center',
    borderWidth: 2,
    borderBottomWidth: 4,
  },
  disabled: {
    borderWidth: 2,
    borderBottomWidth: 0,
    backgroundColor: theme.colors.borderColorDefault,
    borderColor: theme.colors.borderColorDefault,
  },
  labelDisabled: {
    color: theme.colors.alto,
  },
  default: {
    borderColor: theme.colors.borderColorDefault,
    backgroundColor: theme.colors.white,
  },
  primary: {
    borderColor: theme.colors.primary,
    borderBottomColor: theme.colors.borderColorPrimary,
    backgroundColor: theme.colors.primary,
  },
  danger: {
    borderColor: theme.colors.cinnabar,
    borderBottomColor: theme.colors.persianRed,
    backgroundColor: theme.colors.cinnabar,
  },
  secondary: {
    borderColor: theme.colors.secondary,
    borderBottomColor: theme.colors.borderColorSecondary,
    backgroundColor: theme.colors.secondary,
  },
  label: {
    fontFamily: 'DinRoundPro-Bold',
    fontSize: 17,
    lineHeight: 20,
    textAlign: 'center',
    textTransform: 'uppercase',
    color: theme.colors.muted,
  },
});
